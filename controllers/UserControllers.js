const User = require('../models/User');
const Product = require('../models/Product')
const bcrypt = require('bcrypt');
const auth = require('../auth')

//Check if email already exists
//will be used for validation during user registration
module.exports.checkEmail = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true //Email exists.
		} else {
			return false //No registered email yet.
		}
	})
}

//User Registration
module.exports.registerUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then((result) => {
		if (result == null) {
			let newUser = new User ({
				firstName: reqBody.firstName,
				lastName: reqBody.lastName,
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				mobileNo: reqBody.mobileNo
			});
			//console.log(newUser)
			return newUser.save().then((savedUser, error) => {
				if (error) {
					//console.log(error)
					return false
				} else {
					return true //"User is successfully registered."
				}
			})
		
		} else {
			return false //"Email already exists."
		}
	})
};

//User Login/Authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => { 
		if(result === null){
			return false //"Email is not found."
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			} else {
				return false //"Email or Password did not match."
			}
		}
		})
};

//Get profile of all users
module.exports.getUser = () => {
	return User.find({}).then(result => {
		result.password = ""
		return result
	})
}	

//Get one user's profile
module.exports.getOneUser = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = ""
		return result
	})
}

//Updating of user's profile
module.exports.userUpdate = (data) => {
	
	const updatedProfile = ({	
		firstName: data.updateUser.firstName,
		lastName: data.updateUser.lastName,
		email: data.updateUser.email,
		password: data.hashSync(data.updateUser.password, 10),
		mobileNo: data.updateUser.mobileNo
		})	
	
	return User.findByIdAndUpdate(data.userId, updatedProfile).then((result, error) => {
		if (error) {
			return false //"User data cannot be updated."
		} else {
			return result //`User data is updated successfully.` 
		}
	})
}

//Get my Orders
module.exports.getMyCart = (userId) => {
	return User.findById(userId).then(result => {
		if (result.cart.length < 0) {
			return false //`You have no items in cart`
		} else { 
			return result.cart
		}
	})
}

//Cart Products (user only) 
module.exports.addToCart = (data) => {

	let count = 0;
	let index = 0;

	return Product.findById(data.productId).then(product => {

		return User.findById(data.userId).then(user => {

			for(i = 0; i < user.cart.length; i++) {

				if (user.cart[i].productId !== data.productId) {
					continue
				} else {
					count++
					index = i
				}
			}

			if (count > 0) {
				user.cart[index].quantity++

				return user.save().then((result, error) => {
					if (error) {
						return false
					} else { 
						return true //result
					}
				})
			} else {
				user.cart.push({name: product.name, description: product.description, productId: data.productId, quantity: 1, price: product.price, imageURL: product.imageURL})

				return user.save().then((result, error) => {
					if (error) {
						return false
					} else {
						return true //`Added to Cart`
					}
				})		
			}
		})
	})
}

//Add more products to cart
module.exports.addMoreToCart = (data) => {

	let count = 0;
	let index = 0;

	return Product.findById(data.productId).then(product => {

		return User.findById(data.userId).then(user => {

			for(i = 0; i < user.cart.length; i++) {

				if (user.cart[i].productId !== data.productId) {
					continue
				} else {
					count++
					index = i
				}
			}

			if (count > 0) {
				user.cart[index].quantity += data.quantity

				return user.save().then((result, error) => {
					if (error) {
						return false
					} else 
						return true
				})
			} else {
				user.cart.push({name: product.name, description: product.description, productId: data.productId, quantity: data.quantity, price: product.price, imageURL: product.imageURL})

				return user.save().then((result, error) => {
					if (error) {
						return false
					} else {
						return true // `Added to Cart`
					}
				})		
			}
		})
	})
}

//Delete Cart Products (user only) 
module.exports.removeProduct = (data) => {

	return User.findById(data.userId).then(user => {

		for(i = 0; i < user.cart.length; i++) {
			if (user.cart[i].productId === data.productId) {
				user.cart.splice(i, 1)

				return user.save().then((result, error) => {
					if (error) {
						return false  
					} else {
						return true
					}
				})	
			}
		} 
	})
}

//Delete All Cart Products (user only) 
module.exports.deleteCart = (data) => {

	return User.findById(data.userId).then(user => {

		user.cart.splice(0, user.cart.length)

		return user.save().then((result, error) => {
			if (error) {
				return false  
			} else {
				return true
			}
		})	
	})
}

//Edit Quantity of Cart Products (user only) 
module.exports.updateCart = (data) => {

	return User.findById(data.userId).then(user => {

		for(i = 0; i < user.cart.length; i++) {
			if (user.cart[i].productId === data.productId) {

				user.cart[i].quantity = data.quantity

				return user.save().then((result, error) => {
					if (error) {
						return false  
					} else {
						return true
					}
				})	
			}
		} 

	})
}

//Get Total of Cart Products (user only) 
module.exports.totalBill = (data) => {

	let arrayTotal = [];

	return User.findById(data.userId).then(user => {
		if (user.cart.length > 0) {
			user.cart.map(product => arrayTotal.push(product.quantity * product.price))
			const totalPrice = arrayTotal.reduce((q1, q2) => q1 + q2)
			return {totalPrice: totalPrice}
		} else {
			return false
		}
	})
}

//Checkout (user only)
module.exports.checkoutOrder = async (data) => {
	
	let productId = [];
	let quantityProductId = [];
	let priceProductId = [];
	let totalPrice = 0;
			 	
	
	let isProductUpdated = false;

	let isUserUpdated = await User.findById(data.userId).then(user => {

		if (user.cart.length > 0) {

			//Containing product ids of carted products
			for (i = 0; i < user.cart.length; i++) {
				productId.push(user.cart[i].productId)
				quantityProductId.push(user.cart[i].quantity)
				priceProductId.push(user.cart[i].unitPrice)

				totalPrice += (user.cart[i].quantity * user.cart[i].price)
			}

			let order = {
				userId: data.userId,
				products: user.cart,
				totalBill: totalBill,
				shippingAddress: data.shippingAddress
			}

			user.order.push(order)

			user.cart = []

			return user.save().then((result, error) => {
				if (error) {
					return false
				} else {
					return true
				}
			})

		} else {
			return false
		}

	})

	for (i = 0; i < productId.length; i++) {

		let productUpdate = await Product.findById(productId[i]).then(product => {

			let customer = {
				userId: data.userId,
				quantity: quantityProductId[i]
			}


			product.ordered.push(customer)
			product.stocks -= customer.quantity

			if (product.stocks === 0) {
				product.isActive = false
			}
			
			return product.save().then((result, error) => {
				if (error) {
					return false
				} else {
					return true //`Product Updated!`
				}
			})
		})

		isProductUpdated = productUpdate
	}

	if (isUserUpdated !== false && isProductUpdated !==false) {
		return true
	} else {
		return false
	}
}

//Retrieving All Orders (admin)
module.exports.getAllOrders = () => {
	return User.find({"isAdmin": false}).then(result => {

		let orderContainer = [];
		let allOrders = [];
		
		if (result.length < 0) {
			return false
		} else {
			orderContainer = result.map(user => user.order.map(order => allOrders.push(order)))
			return allOrders
		}
	})
} 
 
//Deleting an order (admin)
module.exports.deleteOrder = (reqParams) => {
	return User.find({"isAdmin": false}).then(result => {

		let orders;

		for (i = 0; i < result.length; i++) {
			
			if (result[i].order.length > 0) {
				for (x = 0; x < result[i].order.length; x++) {
					if (result[i].order[x].id === reqParams.orderId) {

						orders = result[i].order[x]

						result[i].order.splice(x, 1)

						return result[i].save().then((result, error) => {
							if (error) {
								return false
							} else {
								return {alert: `Order Deleted.`}
							}
						})

					} else {
						continue
					}
				} 
			} else {
				continue
			}
		}

		if (orders === undefined) {
			return {alert: `Order does not exist`}
		}
	})
}

//Get my Orders
module.exports.getAllMyOrders = (userId) => {
	return User.findById(userId).then(result => {
		if (result.order.length <= 0) {
			return false
		} else { 
			return result.order
		}
	})
}