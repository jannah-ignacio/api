const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

//Route for checking if email exists
router.post("/checkEmail", (req, res) => {
	
	userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController));
})

//Route for User Registration
router.post("/register", (req, res) => {
	
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//Routes for User Authentication
router.post("/login", (req, res) => {
	
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
//The "auth.verify" will acts as a middleware to ensure that the user is logged in before they can get the details
router.get("/details", auth.verify, (req, res) => {
		
	userController.getUser().then(resultFromController => res.send(resultFromController))
});

// Route for retrieving one user's profile
router.get("/", auth.verifyUser, (req, res) => {

	const userId = auth.decode(req.headers.authorization).id 
	
	userController.getOneUser(userId).then(resultFromController => res.send(resultFromController))
});

// Route for updating user's profile
router.put("/update", auth.verifyUser, (req, res) => {
	
	const data = {
		userId: auth.decode(req.headers.authorization).id,
		updateUser: req.body
	}

	userController.userUpdate(data).then(resultFromController => res.send(resultFromController))
});

//Router for retrieving cart (User only)
router.get('/myCart', auth.verifyUser, (req, res) => {

	const userId = auth.decode(req.headers.authorization).id 

	userController.getMyCart(userId).then(resultFromController => res.send(resultFromController))
})

// Route for creating quick add to cart by User only
router.post("/:productId/cart", auth.verifyUser, (req, res) => {

	let data = {
		productId : req.params.productId,
		userId : auth.decode(req.headers.authorization).id
	}

	userController.addToCart(data).then(resultFromController => res.send(resultFromController))
});

// Route for add to cart on Cart Card by User only
router.post("/:productId/addmoretocart", auth.verifyUser, (req, res) => {

	let data = {
		productId: req.params.productId,
		userId: auth.decode(req.headers.authorization).id,
		quantity: req.body.quantity
	}

	userController.addMoreToCart(data).then(resultFromController => res.send(resultFromController))
});

// Route for removing item on cart by User only
router.delete("/:productId/cart", auth.verifyUser, (req, res) => {

	let data = {
		productId : req.params.productId,
		userId : auth.decode(req.headers.authorization).id
	}	

	userController.removeProduct(data).then(resultFromController => res.send(resultFromController))
})

// Route for deleting cart by User only 
router.delete("/cart", auth.verifyUser, (req, res) => {

	let userId = auth.decode(req.headers.authorization).id

	userController.deleteCart(userId).then(resultFromController => res.send(resultFromController))
});

// Route for adding product to existing cart by User only
router.put("/:productId/cart", auth.verifyUser, (req, res) => {

	let data = {
		productId: req.params.productId,
		userId: auth.decode(req.headers.authorization).id,
		quantity: quantity
	}

	userController.updateCart(data).then(resultFromController => res.send(resultFromController))
});

// Route for total bill on cart by User only
router.get("/totalBill/cart", auth.verifyUser, (req, res) => {

	let userId = auth.decode(req.headers.authorization).id

	userController.totalBill(userId).then(resultFromController => res.send(resultFromController))
});

// Route for checkout by User only
router.post("/checkout", auth.verifyUser, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		shippingAddress: req.body.shippingAddress
	}

	userController.checkoutOrder(data).then(resultFromController => res.send(resultFromController))
});

//Router for retrieving all orders (Admin only)
router.get("/orders", auth.verifyAdmin, (req, res) => {
	
	userController.getAllOrders().then(resultFromController => res.send(resultFromController))
})

//Router for deleting an order (Admin only)
router.delete("/orders/:orderId", auth.verifyAdmin, (req, res) => {
	
	userController.deleteOrder(req.params).then(resultFromController => res.send(resultFromController))
})

//Router for retrieving all orders (User only)
router.get("/myOrders", auth.verifyAdmin, (req, res) => {

	const userId = auth.decode(req.headers.authorization).id 

	userController.getAllMyOrders(userId).then(resultFromController => res.send(resultFromController))
})

module.exports = router;