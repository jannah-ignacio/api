const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const port = 4700;
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const app = express();

mongoose.connect("mongodb+srv://admin:123@course.yg49u.mongodb.net/API_Ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Now connected to MongoDB Atlas"));


app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use("/users", userRoutes);
app.use("/products", productRoutes);

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
});

//https://rocky-savannah-51832.herokuapp.com/ | https://git.heroku.com/rocky-savannah-51832.git
